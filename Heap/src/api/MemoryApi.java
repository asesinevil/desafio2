package api;
import java.util.ArrayList;

import modelo.heapProyecto;

public class MemoryApi implements Api{
	
	private heapProyecto miHeap;
	private ArrayList<Integer> datosIngresados = new ArrayList<Integer>();
	
	@Override
	public void crearHeap(Integer cantidad) {
		this.miHeap = new heapProyecto(cantidad);
	}
	
	public void agregarValor(Integer valor) {
		datosIngresados.add(valor);
	}
	
	public void crearHeapOrdeanda() {
		miHeap.AgregarArrayDeValores(datosIngresados);
	}
	
	@Override
	public Integer cantidadElementos() {
		return miHeap.cantidadMaxima();
	}
	
	@Override
	public ArrayList<Integer> listarHeapCreadaPorUsuario() {
		return (this.datosIngresados);
	}
	
	public ArrayList<Integer> listarHeapOrdenada(){
		return(miHeap.devolverHeap());
	}
	
	public Integer obtenerNumero(Integer i){
		return(miHeap.obtenerValor(i));
	}
	
	public void modificarArbol(Integer index) {
		miHeap.crearMinHeap(index);
	}
}