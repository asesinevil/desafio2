package api;

import java.util.ArrayList;


public interface Api {
	
	public void crearHeap(Integer cantidad);
	
	public void agregarValor(Integer valor);
	
	public void crearHeapOrdeanda();
	
	public Integer cantidadElementos();
	
	public ArrayList<Integer> listarHeapCreadaPorUsuario();
	
	public ArrayList<Integer> listarHeapOrdenada();
	
	public Integer obtenerNumero(Integer i);
	
	public void modificarArbol(Integer index);
}
