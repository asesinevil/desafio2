package vista;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import api.Api;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JEditorPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class armarArbol extends JFrame {

	private JPanel contentPane;
	
	
	
	
	private static int i = 3;
	private static int j = 0;
	private static int encontrado = 0;
	private ArrayList<JLabel> correctas = new ArrayList<JLabel>();
	public armarArbol(Api memoryApi) {
	
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1270, 765);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(4, 2, 0, 0));
		JLabel lblNewLabel = new JLabel(i + "");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(i!=0)
				 i--;
				 lblNewLabel.setText(i + "");
			}
		});
		JPanel panelBajo = new JPanel();
		JPanel panelMasBajo = new JPanel();
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 27));
		lblNewLabel.setBounds(0, 0, 20, 23);
		JPanel panelMedio = new JPanel();
		for(Integer cadena: memoryApi.listarHeapCreadaPorUsuario())
		{
			JLabel lblNumero = new JLabel(cadena + "");
			lblNumero.setName(j + "");
			lblNumero.setBorder(new CompoundBorder(new EtchedBorder(),new LineBorder(Color.black)) );
			lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 30));
			lblNumero.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					

					lblNewLabel.setText(i + "");//Dibujar contador;					
					JLabel nuevo = (JLabel)e.getComponent();
					if(i!=0)
					{
					if(memoryApi.obtenerNumero(encontrado)==Integer.parseInt(nuevo.getText()))
					{
						System.out.println("true");
						correctas.get(encontrado).setVisible(true);
						encontrado++;
			
					}
					else
					{
						i--;
						lblNewLabel.setText(i + "");
						JLabel equivocado = new JLabel(nuevo.getText());
						equivocado.setBorder(new CompoundBorder(new EtchedBorder(),new LineBorder(Color.red)));
						equivocado.setFont(new Font("Tahoma", Font.PLAIN, 30));
						panelMasBajo.add(equivocado);
					}
					}
					else
					{
						agregarDialog(contentPane);
					}
					
				//	System.out.println(memoryApi.obtenerNumero(encontrado) + "y" + Integer.parseInt(nuevo.getText()));
				}
			});

			
			panelMedio.add(lblNumero);
	
	        j++;		
		}
	
		
		for(Integer cadena: memoryApi.listarHeapOrdenada())
		{
			JLabel lblNumero1 = new JLabel(cadena + "");

			lblNumero1.setBorder(new CompoundBorder(new EtchedBorder(),new LineBorder(Color.green)));
			lblNumero1.setFont(new Font("Tahoma", Font.PLAIN, 30));
			lblNumero1.setVisible(false);
			panelBajo.add(lblNumero1);
			correctas.add(lblNumero1);
		}
	
	
		
		JPanel panelSuperior = new JPanel();
		contentPane.add(panelSuperior);

		panelSuperior.setLayout(null);
		
		JLabel lblTitulo = new JLabel("Representacion grafica de la Heap.");
		lblTitulo.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 21));
		lblTitulo.setBounds(311, 10, 277, 23);
		
		panelSuperior.add(lblTitulo);
		

		panelSuperior.add(lblNewLabel);
		contentPane.add(panelMedio);

		contentPane.add(panelBajo);
		

		contentPane.add(panelMasBajo);

	}
	public void agregarDialog(JPanel contanePane)
	{
	    // for copying style
	    JLabel label = new JLabel();
	    Font font = label.getFont();

	    // create some css from the label's font
	    StringBuffer style = new StringBuffer("font-family:" + font.getFamily() + ";");
	    style.append("font-weight:" + (font.isBold() ? "bold" : "normal") + ";");
	    style.append("font-size:" + font.getSize() + "pt;");

	    // html content
	    JEditorPane ep = new JEditorPane("text/html", "<html><body style=\"" + style + "\">" //
	            + "Refuerze el contenido y vuelva a intentar: <a href=\"https://sites.google.com/site/arbolesheap/\">Link</a>" //
	            + "</body></html>");

	    // handle link events
	    ep.addHyperlinkListener(new HyperlinkListener()
	    {
	        @Override
	        public void hyperlinkUpdate(HyperlinkEvent e)
	        {
	            if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))
					try {
						Desktop.getDesktop().browse(e.getURL().toURI());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (URISyntaxException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} // roll your own link launcher or use Desktop if J6+
	        }
	    });
	    ep.setEditable(false);
	    ep.setBackground(label.getBackground());

	    // show
	    JOptionPane.showMessageDialog(contanePane, ep,"Demasiados Intentos!",JOptionPane.WARNING_MESSAGE);
	}
}


