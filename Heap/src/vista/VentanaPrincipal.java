package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import api.Api;
import api.MemoryApi;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;
	private final int cantidadMinima = 9;
	private final int cantidadMaxima = 20;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Api miApi = new MemoryApi();
					VentanaPrincipal frame = new VentanaPrincipal(miApi);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaPrincipal(Api memoryApi){

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lblTitulo = new JLabel("Simulador de Heap");
		lblTitulo.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 16));
		lblTitulo.setBounds(157, 10, 119, 29);
		contentPane.add(lblTitulo);
		
		JLabel lblTituloSelect = new JLabel("\u00BFCuantos elementos tendra tu Heap? ");
		lblTituloSelect.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 15));
		lblTituloSelect.setBounds(10, 92, 262, 68);
		contentPane.add(lblTituloSelect);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(319, 119, 46, 21);
		for(int i = cantidadMinima; i <= cantidadMaxima;i++)
		{
			comboBox.addItem(i);
		}
		contentPane.add(comboBox);
		
		JButton btnEnviarHeap = new JButton("Crear Heap");
		btnEnviarHeap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int cantidad = (int)comboBox.getSelectedItem();
				memoryApi.crearHeap(cantidad);
		
				LeerValor nuevaVentana = new LeerValor(memoryApi);
				nuevaVentana.setVisible(true);
				setVisible(false);
		
			}
		});
		btnEnviarHeap.setFont(new Font("Verdana", Font.PLAIN, 13));
		btnEnviarHeap.setBounds(286, 173, 119, 29);
		contentPane.add(btnEnviarHeap);
		
	}
}

