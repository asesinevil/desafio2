package vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import api.Api;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JToggleButton;

public class LeerValor extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel labelParaInfo;
	private JButton btnFin;
	
	private Integer cantidad;
	private Integer max;
	private Integer ultimoPadre;
	private Integer movX;
	private Integer i = 0;
	private Integer posBase = 48; 
	
	private ArrayList<JLabel> misNumeros = new ArrayList<JLabel>();
	private ArrayList<JToggleButton> nodosArbol = new ArrayList<JToggleButton>();
	
	private JButton btnAgregarAleatorio;
	private JTextField txtSeleccion;

	public LeerValor(Api memoryApi) {
		
		cantidad = memoryApi.cantidadElementos();
		max = cantidad;
		movX = 0;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1100,680);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 240, 240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lblTitulo = new JLabel("Ingrese valores del 1 al 99 para su Heap:");
		lblTitulo.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 16));
		lblTitulo.setBounds(72, 40, 291, 29);
		contentPane.add(lblTitulo);
		
		
		
		labelParaInfo = new JLabel("New Label");
		labelParaInfo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		labelParaInfo.setHorizontalAlignment(SwingConstants.CENTER);
		labelParaInfo.setBounds(10, 40, 1066, 44);
		contentPane.add(labelParaInfo);
		labelParaInfo.setVisible(false);
		
		txtSeleccion = new JTextField();
		txtSeleccion.setBounds(754, 129, 113, 29);
		contentPane.add(txtSeleccion);
		txtSeleccion.setColumns(10);
		txtSeleccion.setVisible(false);
		
		
		JLabel lblSeleccion = new JLabel("Ingrese aqui:");
		lblSeleccion.setBounds(648, 133, 84, 21);
		contentPane.add(lblSeleccion);
		lblSeleccion.setVisible(false);
		
		btnAgregarAleatorio = new JButton("Agregar valores " + "aleatoriamente.");
		btnAgregarAleatorio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int valor = 0;
				int max = cantidad;
				int res = 0;
				
				btnAgregarAleatorio.setEnabled(false);
				
				for(int i = 0; i < max; i++)
				{    
					valor = (int)(Math.random()*(99-1+1)+1);
					
					nodosArbol.get(i).setText("" + valor);
					
					memoryApi.agregarValor(valor);
					cantidad--;
					
					 JLabel nuevo = misNumeros.get(i);
					 if(saltarLinea(nuevo))
					 {
						 nuevo.setBounds(nuevo.getBounds().x + res, nuevo.getBounds().y, nuevo.getBounds().width, nuevo.getBounds().height);
						 res = res + 25;
					 }
					 nuevo.setText(valor + "");
					 nuevo.setVisible(true);

				}
				
				memoryApi.crearHeapOrdeanda();
				
				if(JOptionPane.showConfirmDialog(null, "Se ha cargado a tope la heap. \n Comenzemos a ordenar", null, 
						JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE)==0) {
					btnAgregarAleatorio.setVisible(false);
					textField.setVisible(false);
					lblTitulo.setVisible(false);
					btnFin.setVisible(true);
					ultimoPadre = max/2;
					identificarDesordenados(labelParaInfo);
					
					txtSeleccion.setVisible(true);
					lblSeleccion.setVisible(true);
				}
			}
		});
		
		btnAgregarAleatorio.setBounds(762, 38, 236, 34);
		contentPane.add(btnAgregarAleatorio);

		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer valor = -1;
				
				btnAgregarAleatorio.setEnabled(false);
				
				if(seguirLeyendo(cantidad))
					{
					try {
						nodosArbol.get(i).setText(textField.getText());
						
						valor = Integer.parseInt(textField.getText());
						cantidad--;
				 
						memoryApi.agregarValor(valor);
						JLabel nuevo = misNumeros.get(i);
						nuevo.setText(valor + "");
						nuevo.setVisible(true);
						i++;
						textField.setText(null);
						
				 
					}
					catch(NumberFormatException error)
					{
						System.out.println("Invalido" + error.getMessage());
					}
					System.out.println(valor);
				}
				else
				{

				if(JOptionPane.showConfirmDialog(null, "Se ha cargado a tope la heap", null, 
						JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE)==0) {
						memoryApi.crearHeapOrdeanda();
						btnAgregarAleatorio.setVisible(false);
						textField.setVisible(false);
						lblTitulo.setVisible(false);
						btnFin.setVisible(true);		
						ultimoPadre = max/2;

						identificarDesordenados(labelParaInfo);
						
						txtSeleccion.setVisible(true);
						lblSeleccion.setVisible(true);
					}	
				}			
			}
		});
		
		
		
		btnFin = new JButton("Continuar");
		btnFin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Integer valorIngresado = Integer.parseInt(txtSeleccion.getText());
				System.out.println(ultimoPadre);
				System.out.println(memoryApi.obtenerNumero(ultimoPadre));
				
				if(memoryApi.obtenerNumero(ultimoPadre) == valorIngresado) {
					
					mensajeSeleccionCorrecta(labelParaInfo);
					memoryApi.modificarArbol(ultimoPadre);
					ultimoPadre--;
					
				}else {
					mensajeSeleccionErronea(labelParaInfo);
				}
				
				txtSeleccion.setText(null);
			}
		});
		
		btnFin.setBackground(new Color(0, 128, 192));
		btnFin.setOpaque(true);
		btnFin.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnFin.setBounds(946, 589, 113, 44);
		contentPane.add(btnFin);
		btnFin.setVisible(false);
		
		
		
		
		textField.setBounds(373, 44, 125, 23);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btVolver = new JButton("Volver");
		btVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaPrincipal miVentana = new VentanaPrincipal(memoryApi);
				setVisible(false);
				miVentana.setVisible(true);
			}
		});
		btVolver.setBounds(10, 612, 85, 21);
		contentPane.add(btVolver);
		
		JToggleButton label1 = new JToggleButton("N");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setBackground(new Color(0, 128, 192));
		label1.setOpaque(true);
		label1.setBounds(583, 229-40, 47, 35);
		contentPane.add(label1);
		label1.setVisible(false);
		this.nodosArbol.add(label1);
		
		JToggleButton label2 = new JToggleButton("N");
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setBackground(new Color(0, 128, 192));
		label2.setOpaque(true);
		label2.setBounds(318, 289-40, 47, 35);
		contentPane.add(label2);
		label2.setVisible(false);
		this.nodosArbol.add(label2);
		
		JToggleButton label3 = new JToggleButton("N");
		label3.setBackground(new Color(0, 128, 192));
		label3.setOpaque(true);
		label3.setBounds(820, 289-40, 47, 35);
		contentPane.add(label3);
		label3.setVisible(false);
		this.nodosArbol.add(label3);
		
		JToggleButton label4 = new JToggleButton("N");
		label4.setHorizontalAlignment(SwingConstants.CENTER);
		label4.setBackground(new Color(0, 128, 192));
		label4.setOpaque(true);
		label4.setBounds(152, 337-40, 47, 35);
		contentPane.add(label4);
		label4.setVisible(false);
		this.nodosArbol.add(label4);
		
		JToggleButton label5 = new JToggleButton("N");
		label5.setHorizontalAlignment(SwingConstants.CENTER);
		label5.setBackground(new Color(0, 128, 192));
		label5.setOpaque(true);
		label5.setBounds(476, 337-40, 47, 35);
		contentPane.add(label5);
		label5.setVisible(false);
		this.nodosArbol.add(label5);
		
		JToggleButton label6 = new JToggleButton("N");
		label6.setHorizontalAlignment(SwingConstants.CENTER);
		label6.setBackground(new Color(0, 128, 192));
		label6.setOpaque(true);
		label6.setBounds(685, 337-40, 47, 35);
		contentPane.add(label6);
		label6.setVisible(false);
		this.nodosArbol.add(label6);
		
		JToggleButton label7 = new JToggleButton("N");
		label7.setHorizontalAlignment(SwingConstants.CENTER);
		label7.setBackground(new Color(0, 128, 192));
		label7.setOpaque(true);
		label7.setBounds(934, 337-40, 47, 35);
		contentPane.add(label7);
		label7.setVisible(false);
		this.nodosArbol.add(label7);
		
		JToggleButton label8 = new JToggleButton("N");
		label8.setHorizontalAlignment(SwingConstants.CENTER);
		label8.setBackground(new Color(0, 128, 192));
		label8.setOpaque(true);
		label8.setBounds(64, 390-40, 47, 35);
		contentPane.add(label8);
		label8.setVisible(false);
		this.nodosArbol.add(label8);
		
		JToggleButton label9 = new JToggleButton("N");
		label9.setHorizontalAlignment(SwingConstants.CENTER);
		label9.setBackground(new Color(0, 128, 192));
		label9.setOpaque(true);
		label9.setBounds(243, 390-40, 47, 35);
		contentPane.add(label9);
		label9.setVisible(false);
		this.nodosArbol.add(label9);
		
		JToggleButton label10 = new JToggleButton("N");
		label10.setHorizontalAlignment(SwingConstants.CENTER);
		label10.setBackground(new Color(0, 128, 192));
		label10.setOpaque(true);
		label10.setBounds(401, 390-40, 47, 35);
		contentPane.add(label10);
		label10.setVisible(false);
		this.nodosArbol.add(label10);
		
		JToggleButton label11 = new JToggleButton("N");
		label11.setHorizontalAlignment(SwingConstants.CENTER);
		label11.setBackground(new Color(0, 128, 192));
		label11.setOpaque(true);
		label11.setBounds(556, 390-40, 47, 35);
		contentPane.add(label11);
		label11.setVisible(false);
		this.nodosArbol.add(label11);
		
		JToggleButton label12 = new JToggleButton("N");
		label12.setHorizontalAlignment(SwingConstants.CENTER);
		label12.setBackground(new Color(0, 128, 192));
		label12.setOpaque(true);
		label12.setBounds(611, 390-40, 47, 35);
		contentPane.add(label12);
		label12.setVisible(false);
		this.nodosArbol.add(label12);
		
		JToggleButton label13 = new JToggleButton("N");
		label13.setHorizontalAlignment(SwingConstants.CENTER);
		label13.setBackground(new Color(0, 128, 192));
		label13.setOpaque(true);
		label13.setBounds(762, 390-40, 47, 35);
		contentPane.add(label13);
		label13.setVisible(false);
		this.nodosArbol.add(label13);
		
		JToggleButton label14 = new JToggleButton("N");
		label14.setHorizontalAlignment(SwingConstants.CENTER);
		label14.setBackground(new Color(0, 128, 192));
		label14.setOpaque(true);
		label14.setBounds(866, 390-40, 47, 35);
		contentPane.add(label14);
		label14.setVisible(false);
		this.nodosArbol.add(label14);
		
		JToggleButton label15 = new JToggleButton("N");
		label15.setHorizontalAlignment(SwingConstants.CENTER);
		label15.setBackground(new Color(0, 128, 192));
		label15.setOpaque(true);
		label15.setBounds(1012, 390-40, 47, 35);
		contentPane.add(label15);
		label15.setVisible(false);
		this.nodosArbol.add(label15);
		
		JToggleButton label16 = new JToggleButton("N");
		label16.setForeground(new Color(0, 0, 0));
		label16.setHorizontalAlignment(SwingConstants.CENTER);
		label16.setBackground(new Color(0, 128, 192));
		label16.setOpaque(true);
		label16.setBounds(10, 456-40, 47, 35);
		contentPane.add(label16);
		label16.setVisible(false);
		this.nodosArbol.add(label16);
		
		JToggleButton label17 = new JToggleButton("N");
		label17.setHorizontalAlignment(SwingConstants.CENTER);
		label17.setBackground(new Color(0, 128, 192));
		label17.setOpaque(true);
		label17.setBounds(119, 456-40, 47, 35);
		contentPane.add(label17);
		label17.setVisible(false);
		this.nodosArbol.add(label17);
		
		JToggleButton label18 = new JToggleButton("N");
		label18.setHorizontalAlignment(SwingConstants.CENTER);
		label18.setBackground(new Color(0, 128, 192));
		label18.setOpaque(true);
		label18.setBounds(188, 456-40, 47, 35);
		contentPane.add(label18);
		label18.setVisible(false);
		this.nodosArbol.add(label18);
		
		JToggleButton label19 = new JToggleButton("N");
		label19.setHorizontalAlignment(SwingConstants.CENTER);
		label19.setBackground(new Color(0, 128, 192));
		label19.setOpaque(true);
		label19.setBounds(294, 456-40, 47, 35);
		contentPane.add(label19);
		label19.setVisible(false);
		this.nodosArbol.add(label19);
		
		JToggleButton label20 = new JToggleButton("N");
		label20.setHorizontalAlignment(SwingConstants.CENTER);
		label20.setBackground(new Color(0, 128, 192));
		label20.setOpaque(true);
		label20.setBounds(354, 456-40, 47, 35);
		contentPane.add(label20);
		label20.setVisible(false);
		this.nodosArbol.add(label20);
		
		
		
		for(int i = 0; i < max; i++)
		{
			JLabel lblNumero = new JLabel();
			lblNumero.setBorder(new CompoundBorder(new EtchedBorder(),new LineBorder(Color.black)) );
			lblNumero.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblNumero.setBounds(posBase + movX, 134, 54, 29);
			contentPane.add(lblNumero);
			movX = movX + 25;
			misNumeros.add(lblNumero);
			
			this.nodosArbol.get(i).setVisible(true);
		}

		
}
	
	
	private void identificarDesordenados (JLabel unLabel) {
		unLabel.setText("Ingreso completo – Elija por cual nodo "
				+ "debemos comenzar y hacé clic en 'Continuar'");
		unLabel.setVisible(true);
	}
	
//	private void mensajeError(JLabel unLabel) {
//		unLabel.setText("");
//	}
	
	private void mensajeSeleccionCorrecta(JLabel unLabel) {
		unLabel.setText("¡¡Seleccion correcta, continuemos con el siguiente nodo!!");
		unLabel.setVisible(true);
	}
	
	private void mensajeSeleccionErronea(JLabel unLabel) {
		unLabel.setText("Seleccion no es correcta. Recorda que hay una formula y un criterio para crear la Heap!");
		unLabel.setVisible(true);
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Boolean seguirLeyendo(Integer cantidad)
	{
		if(cantidad > 0)
		{
			return(true);
			
		}
		else
		{
			return(false);
		}
	}
	public Boolean saltarLinea(JLabel miLabel)
	{
		if(saltoDeLinea((int)miLabel.getBounds().x))
		{	
			miLabel.setBounds(posBase, miLabel.getBounds().y + 20, miLabel.getBounds().width, miLabel.getBounds().height);
			System.out.println(miLabel.getBounds().x);
			return(true);
		}
		else
		{
			return(false);
		}
	}
	public Boolean saltoDeLinea(Integer x)
	{
		if(this.contentPane.getBounds().width > x)
		{
			return(false);
		}
		else
		{
			return(true);
		}
	}
}

