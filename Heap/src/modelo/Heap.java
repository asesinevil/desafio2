package modelo;

import java.util.ArrayList;
import java.util.Collections;

public class Heap {
	private ArrayList<Integer> miHeap;
	private ArrayList<Integer> miHeapOrdenada;
	private ArbolBinario miArbol = null;
	private Integer max;
	
	public void llenarHeap()
	{

		
			miHeapOrdenada = miHeap;
			Collections.reverse(miHeapOrdenada);
		
	}
	public Heap (Integer cantidad)
	{
		this.miHeap = new ArrayList<Integer>(cantidad);
		this.miArbol = new ArbolBinario();
		System.out.println(cantidad);
		this.max = cantidad;
	}
	public void agregarValor(Integer valor)
	{
		if(!llegoTope())
		{
		 miHeap.add(valor);
		 miArbol.insertarNodo(new NodoBinario(valor)); //ACA
		}
	}
	public ArrayList<Integer> listarHeap()
	{
		return(this.miHeap);
	}
	
	public void ordenarHeap()
	{}
	public ArrayList<Integer> listarHeapOrdenada()
	{   llenarHeap();
		return(miHeapOrdenada);}
	public Integer obtenerHeapOrdenada(Integer i)
	{return(miHeapOrdenada.get(i));}
	public Boolean llegoTope()
	{
		Boolean estado = false;
		
		if(miArbol.obtenerTam()>=max)
		{
			estado = true;
		}
		return(estado);
	}
	public Integer cantidadMaxima()
	{return(this.max);}
	
}
