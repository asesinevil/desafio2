package modelo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class heapProyecto {

	private ArrayList<Integer> datos;
	private int cantElementos;

	
	public heapProyecto(int tamaño) {
		this.datos = new ArrayList<Integer>(tamaño+1);
		this.datos.add(999);
		this.cantElementos = tamaño;
	}

	
	public void AgregarArrayDeValores(ArrayList<Integer> nuevosDatos) {
		
		for(Integer n: nuevosDatos) {
			this.datos.add(n);
		}
		
		this.cantElementos = (this.datos.size()-1);
		
		Integer index = this.cantElementos/2;
		
		while(index > 0) {
			
			this.crearMinHeap(index);
			index--;
		}
	}
	
	
	public void crearMinHeap(Integer index) {
		
		if(!this.esHoja(index)) {
			
			Integer padre = datos.get(index);
			Integer hijoIzq = datos.get(2*index);
			Integer hijoDer = datos.get((2*index)+1);
			
			if(hijoIzq < hijoDer) {
				
				if(hijoIzq < padre) {
					
					this.intercambiar(index, (2*index));
					
					this.crearMinHeap((2*index));
				}
			}
			else if(hijoIzq > hijoDer) {
				
				if(hijoDer < padre) {
					
					this.intercambiar(index, ((2*index)+1));
					
					this.crearMinHeap((2*index)+1);
				}
			}
		}
	}
	
	
	private void intercambiar(Integer index1, Integer index2) {
		
		Integer temp = datos.get(index1);
		datos.set(index1, (datos.get(index2)));
		datos.set(index2, temp);
	}
	
	
	private boolean esHoja(Integer index) {
		return (index > this.cantElementos/2);
	}
	
	
	public Integer cantidadMaxima() {
		return(this.cantElementos);
	}
	
	
	public ArrayList<Integer> devolverHeap(){
		return(this.datos);
	}
	
	
	public Integer obtenerValor(int posicion) {
		System.out.println(this.datos.get(posicion));
		return(this.datos.get(posicion));
	}
	
	
	
	
	private boolean esVacia() {
		if(this.cantElementos == 0) {
			return true;
		}
		return false;
	}
	
	public void agregar(Integer dato){
		
		if(!this.esVacia()) {
			percolate_up(dato);
			this.cantElementos++;
		}
		else {
			this.datos.add(1, dato);
			this.cantElementos++;
		}
	}
	
	public Integer tope() {
		if(!this.esVacia()) {
			Integer e = this.datos.get(1);
			
			this.datos.add(1, this.datos.get(cantElementos));
			
			this.cantElementos--;
			this.percolate_down();
			
			return e;
		}
		return null;
	}
	
	
	private void percolate_up(Integer dato) {
		int N = this.cantElementos;
		
		while((this.datos.get(N/2) > dato) && (N != 1)) {
			
			if(this.datos.get(N) <= this.datos.get(N/2)){
				N = N/2;
			}
		}
		
		this.datos.add(N, dato);
	}
	
	private void percolate_down() {
		Integer candidato = this.datos.get(cantElementos);
		boolean stopPerc = false;
		int p = 1; 
		
		while((2*p <= this.cantElementos) && (!stopPerc)) {
			
			int h_min = 2*p;
			
			if(h_min != this.cantElementos) {
				
				if(this.datos.get(h_min+1) < this.datos.get(h_min)) {
					h_min++;
				}
			}
			
			if(candidato > this.datos.get(h_min)) {
				this.datos.add(p, this.datos.get(h_min)); 
				p = h_min;
			}else {
				stopPerc = true;
			}
		}
		
		this.datos.add(p, candidato);
	}
	
	
	
	
	private Boolean isNull(Object var)
	{   
		if(var == null){
			return(true);
		}
		else {
			return(false);
		}
	}
	
	
	public void mostrarDatos() {
		for(int i=1; i<=this.cantElementos; i++) {
			System.out.println(this.datos.get(i));
		}
	}
}

