package modelo;

public class NodoBinario {
	private Object dato;
	private NodoBinario hijoIzquierdo = null;
	private NodoBinario hijoDerecho = null;
	
	public NodoBinario(Object dato)
	{
		this.dato = dato;
	}
	public Object getDato()
	{
		return(this.dato);
	}
	public NodoBinario getHijoIzquierdo()
	{
		return(hijoIzquierdo);
	}
	public NodoBinario getHijoDerecho()
	{
		return(hijoDerecho);
	}
	public void setDato(Object dato)
	{
		this.dato = dato;
	}
	public void setHijoIzquierdo(NodoBinario unHijo)
	{
		this.hijoIzquierdo = unHijo;
	}
	public void setHijoDerecho(NodoBinario unHijo)
	{
		this.hijoDerecho = unHijo;
	}
}
