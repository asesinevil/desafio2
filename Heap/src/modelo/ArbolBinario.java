package modelo;
import java.lang.Comparable;
import java.util.ArrayList;
import java.util.Comparator;

public class ArbolBinario implements  Comparator, Comparable{
	private NodoBinario raiz;
	
	public ArbolBinario()
	{
		this.raiz = null;
	}
	public ArbolBinario(Object nodo)
	{  
		this.raiz = new NodoBinario(nodo);
	}
	private ArbolBinario(NodoBinario nodo)
	{
		this.raiz = nodo;
	}
	public NodoBinario getRaiz()
	{
		return(this.raiz);
	}
	public Object getDatoRaiz()
	{
		return(this.raiz.getDato());
	}
	public ArbolBinario getHijoIzquierdo()
	{   ArbolBinario subArbolIz = new ArbolBinario(this.raiz.getHijoIzquierdo());
		return(subArbolIz);
	}
	public ArbolBinario getHijoDerecho()
	{ 
		ArbolBinario subArbolDer = new ArbolBinario(this.raiz.getHijoDerecho());
	return(subArbolDer);
	}
	public void agregarOrdenado(ArbolBinario unHijo)
	{ String a = unHijo.getRaiz().getDato() + "";
	  System.out.println("ACA " +a );	
	  if(this.raiz !=null)
	  {
		if(esMenor(unHijo))
		{   
			if(this.raiz.getHijoIzquierdo()==null)
			{
				this.raiz.setHijoIzquierdo(unHijo.getRaiz());
			}
			else
			{
				this.getHijoIzquierdo().agregarOrdenado(unHijo);
			}
		}
		else
		{
			if(this.raiz.getHijoDerecho()==null)
			{
				this.raiz.setHijoDerecho(raiz);
			}
			else
			{
				this.getHijoDerecho().agregarOrdenado(unHijo);
			}
		}
	  }
	  else
	  {
		  this.raiz = unHijo.getRaiz();
	  }
	}
	
	public void agregarHijoIzquierdo(ArbolBinario unHijo)
	{
		    NodoBinario a = unHijo.getRaiz();
			this.raiz.setHijoIzquierdo(a);
	}
	public void agregarHijoDerecho(ArbolBinario unHijo)
	{   NodoBinario a = unHijo.getRaiz();
		this.raiz.setHijoDerecho(a);
	}
	public void eliminarHijoIzquiero()
	{   
		NodoBinario aux = this.raiz.getHijoIzquierdo();
	    aux = null;
	}
	public void eliminarHijoDerecho()
	{
		NodoBinario aux = this.raiz.getHijoDerecho();
        aux = null;
	}
	@Override
	public int compareTo(Object o) {
		String a = ((String)o);
		String b = this.raiz.getDato() + "";
		System.out.println(a + b);
		return(a.compareTo(b));
	}
	@Override
	public int compare(Object o1, Object o2) {
		
		String b = ((NodoBinario)o1).getDato() + "";
		String a = ((NodoBinario)o2).getDato() + "";

		return(a.compareTo(b));
        
	}

	public void preorder()
	{
		if(this.raiz!=null)
		{   
			System.out.print(this.raiz.getDato());
			this.getHijoIzquierdo().preorder();
			this.getHijoDerecho().preorder();
		}
	}
	public void inorder()
	{
		if(this.raiz!=null)
		{   
			
			this.getHijoIzquierdo().inorder();
			System.out.print(" "+this.raiz.getDato());
			this.getHijoDerecho().inorder();
		}
	}
	public void postorder()
	{
		if(this.raiz!=null)
		{   
			
			this.getHijoIzquierdo().postorder();
			this.getHijoDerecho().postorder();
			System.out.print(this.raiz.getDato());
		}
	}
	
	void insertarNodo(NodoBinario nodo) {
	    if (raiz == null) {
	        raiz = nodo;
	        System.out.println("Inserto la raiz");
	    } else {
	        // Necesitamos encontrar en que posici�n debemos insertar el nodo
	    	NodoBinario aux = raiz;
	 
	        while (aux != null) {
	            // Comprobamos si tenemos que insertarlo ya
	            // Comprobamos si nodo hoja
	            if (aux.getHijoDerecho() == null && aux.getHijoIzquierdo() == null) {
	                if ((int)nodo.getDato() > (int)aux.getDato()) {
	                    // Derecha
	                    System.out.println(nodo.getDato() + " Lo insertamos a la derecha de: " + aux.getDato());
	                    aux.setHijoDerecho(nodo);

	                    aux = null;
	                } else {
	                    // Izquierda
	                    System.out.println(nodo.getDato() + " Lo insertamos a la izquierda de: " + aux.getDato());
	                    aux.setHijoIzquierdo(nodo);
	                    aux = null;
	                }
	            } else if ((int)nodo.getDato() > (int)aux.getDato() && aux.getHijoDerecho() == null) {
	                // Lo insertamos a la derecha
	                System.out.println(nodo.getDato() + " Lo insertamos a la derecha de: " + aux.getDato());
	                aux.setHijoDerecho(nodo);
	                aux = null;
	            } else if ((int)nodo.getDato() < (int)aux.getDato() && aux.getHijoIzquierdo() == null) {
	                // Lo insertamos a la izquierda
	                System.out.println(nodo.getDato() + " Lo insertamos a la izquierda de: " + aux.getDato());
	                aux.setHijoIzquierdo(nodo);
	                aux = null;
	            } else {
	                // Pasamos de nodo
	                if ((int)nodo.getDato() > (int)aux.getDato()) {
	                    aux = aux.getHijoDerecho();
	                } else {
	                    aux = aux.getHijoIzquierdo();
	                }
	            }
	 
	        }
	    }
	}
	
	/*******************************PART TWO************************************/
	public ArrayList<Object> frontera()
	{   ArrayList<Object> miArray = new ArrayList<Object>();
		if(this.raiz!=null)
		{
			if(esHoja())
			{
			 miArray.add(this.raiz.getDato());
			}
			else
			{
				miArray.addAll(this.getHijoDerecho().frontera());
				miArray.addAll(this.getHijoIzquierdo().frontera());		
			}
		}
		return(miArray);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
    public static boolean printLevel(NodoBinario root, int level)
    {
        // caso base
        if (root == null) {
            return false;
        }
 
        if (level == 1)
        {
            System.out.print(root.getDato() + " ");
 
            // devuelve verdadero si al menos un nodo est� presente en un nivel dado
            return true;
        }
 
        boolean left = printLevel(root.getHijoIzquierdo(), level - 1);
        boolean right = printLevel(root.getHijoDerecho(), level - 1);
 
        return left || right;
    }
    
    public Integer obtenerTam()
    {   Integer valor = 0;
        try {
		if(this.raiz!=null)
		{   
			valor++;
			if(this.raiz.getHijoIzquierdo()!=null)
				valor = this.getHijoIzquierdo().obtenerTam();
			
			if(this.raiz.getHijoDerecho()!=null)
			    valor = this.getHijoDerecho().obtenerTam();
		}
        }
        catch(StackOverflowError e)
        {
        	System.out.println(e);
        }
        return(valor);
    }
    // Funci�n para imprimir el recorrido del orden de nivel de un �rbol binario dado
    public void recorrerPorNivel()
    {
        // comienza desde el nivel 1 � hasta la altura del �rbol
        int level = 1;
 
        // ejecutar hasta que printLevel() devuelva falso
        while (printLevel(this.getRaiz(), level)) {
            level++;
        }
    }
     
	public Boolean esHoja()
	{ 
		return((this.raiz.getHijoIzquierdo()==null)&&(this.raiz.getHijoDerecho()==null));
	}
	public Boolean esMenor(ArbolBinario unHijo)
	{   
		Boolean estado = false;
		if(this.raiz!=null)
		{
			estado = compare(unHijo.getRaiz(), this.raiz) < 0;	 
		}
		return(estado);
	}
	public Boolean lleno() //REHACER
	{   Boolean estado = this.completo();
	    ArbolBinario aux = new ArbolBinario(this.getRaiz());
	    ArbolBinario aux2 = new ArbolBinario(this.getRaiz());
	    int i = 0;
	    int j = 0;
	    if(estado)
	    {
	    	while(aux.raiz.getHijoIzquierdo()!=null)
	    	{
	    		i++;
	    		aux.raiz = aux.raiz.getHijoIzquierdo();
	    	}
	    	while(aux2.raiz.getHijoDerecho()!=null)
	    	{   j++;
	    	    aux2.raiz = aux2.raiz.getHijoDerecho();
	    	}
		    if(i==j)
		    {   
		    	estado = true;
		    }
		    else
		    {
		    	estado = false;
		    }
	    }
		return(estado);
	}
	public Boolean completo()
	{   Boolean estado = true;
		if(this.raiz!=null)
	    { 	
			if((this.raiz.getHijoIzquierdo()!=null)&&(this.raiz.getHijoDerecho()!=null))
	    	{  
				estado = this.getHijoIzquierdo().completo();
				estado =this.getHijoDerecho().completo();
		    }	
		    else if((this.raiz.getHijoIzquierdo()==null)&&(this.raiz.getHijoDerecho()==null))
			{   	
		    	estado = this.getHijoIzquierdo().completo();
				estado = this.getHijoDerecho().completo();		
			}
			else
			{  
				estado = false;
				
			}
	    }		
		return(estado);
	}
	public ArbolBinario espejo()
	{   
		if(this.raiz!=null)
		{
			if((this.raiz.getHijoIzquierdo()!=null)&&(this.raiz.getHijoDerecho()!=null))
			{
				ArbolBinario aux = new ArbolBinario(this.raiz.getHijoIzquierdo());
				this.raiz.setHijoIzquierdo(this.raiz.getHijoDerecho());
				this.raiz.setHijoDerecho(aux.raiz); 
				
				
				if(this.raiz.getHijoIzquierdo()!=null)
				{
					this.getHijoIzquierdo().espejo();
				}
				if(this.raiz.getHijoDerecho()!=null)
				{
					this.getHijoDerecho().espejo();
				}
			}
		}
		return(this);
	}
public int alturaArbol(){
		
		int alturaPorIzquierda = 0;
		int alturaPorDerecha = 0;
		
		if(this.raiz!=null){
			alturaPorIzquierda = 1 + this.getHijoIzquierdo().alturaArbol();
			alturaPorDerecha = 1 + this.getHijoDerecho().alturaArbol();
		}
		return (Math.max(alturaPorIzquierda, alturaPorDerecha));
	}

	public void colorear()
	{
		
	}
	public ArbolBinario espejo1()
	{
		ArbolBinario miArbol=new ArbolBinario(invertir(raiz));
		System.out.println("funciona espejo");
		return miArbol;
	}
	
	private NodoBinario invertir(NodoBinario n)
	{
		if(n==null)
			return null;
		NodoBinario temp= n.getHijoIzquierdo();
		n.setHijoIzquierdo(invertir(n.getHijoDerecho()));
		n.setHijoDerecho(invertir(temp));
		return n;
	}
	/////////////////////////////////////////////////////////////PART THREE///////////////////////////////////////////
	/* (1) (3) (5) = 22*/
										public Integer sumatoria(int nivel)
										{   
											int total = 0;
											if(this.raiz!=null)
											{   
											  total  = (int)this.raiz.getDato() * nivel;
											  total += this.getHijoIzquierdo().sumatoria(nivel + 1); 
											  total += this.getHijoDerecho().sumatoria(nivel + 1); 
											}										
											return(total);
										}
}
